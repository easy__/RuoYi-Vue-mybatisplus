package com.ruoyi.project.jxy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.project.jxy.domain.FileHistory;
import com.ruoyi.project.jxy.mapper.FileHistoryMapper;
import com.ruoyi.project.jxy.service.IFileHistoryService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 文件记录Service业务层处理
 *
 * @author ruoyi
 * @date 2020-08-06
 */
@Service
@Transactional
public class FileHistoryServiceImpl extends ServiceImpl<FileHistoryMapper, FileHistory> implements IFileHistoryService {

    @Value("${threadNum}")
    private int threadNum;

    @Override
    public int unZip(Long id) {
        FileHistory fileHistory = baseMapper.selectById(id);
        //未解压
        if (fileHistory != null && "0".equals(fileHistory.getZipFlag())) {
            fileHistory.setZipFlag("1");
            fileHistory.setUpdateTime(new Date());
            fileHistory.setUpdateUser(SecurityUtils.getUsername());
            baseMapper.updateById(fileHistory);

            String zipPath = fileHistory.getPath();
            zipPath = zipPath.replace("/profile", RuoYiConfig.getProfile());
            UnZipThread unZipThread = new UnZipThread(zipPath, threadNum, fileHistory);
            new Thread(unZipThread).start();
            return 1;
        }
        return 0;
    }
}
