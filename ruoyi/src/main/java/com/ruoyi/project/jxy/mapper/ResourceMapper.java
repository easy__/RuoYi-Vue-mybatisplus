package com.ruoyi.project.jxy.mapper;

import com.ruoyi.project.jxy.domain.Resource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 资源管理Mapper接口
 * 
 * @author ruoyi
 * @date 2020-05-21
 */
public interface ResourceMapper extends BaseMapper<Resource>
{

}
