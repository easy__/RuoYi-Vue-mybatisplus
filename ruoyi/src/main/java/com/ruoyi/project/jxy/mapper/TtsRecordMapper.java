package com.ruoyi.project.jxy.mapper;

import com.ruoyi.project.jxy.domain.TtsRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 语音合成记录Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-07
 */
public interface TtsRecordMapper extends BaseMapper<TtsRecord>
{

}
