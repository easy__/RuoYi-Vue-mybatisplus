package com.ruoyi.project.jxy.service;

import com.ruoyi.project.jxy.domain.AsrRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 语音识别记录Service接口
 *
 * @author ruoyi
 * @date 2020-08-10
 */
public interface IAsrRecordService extends IService<AsrRecord> {
    /**
     * @return java.util.List
     * @Description //首页 按主题统计数据
     * @Date 2020/9/17 15:58
     * @Param []
     **/
    public List getThemeChartData();
}
