package com.ruoyi.project.jxy.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.jxy.mapper.ImgRecordMapper;
import com.ruoyi.project.jxy.domain.ImgRecord;
import com.ruoyi.project.jxy.service.IImgRecordService;

/**
 * 图片记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-07
 */
@Service
public class ImgRecordServiceImpl extends ServiceImpl<ImgRecordMapper, ImgRecord> implements IImgRecordService
{

}
