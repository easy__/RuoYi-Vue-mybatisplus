package com.ruoyi.project.jxy.service;

import com.ruoyi.project.jxy.domain.ImgRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 图片记录Service接口
 * 
 * @author ruoyi
 * @date 2020-09-07
 */
public interface IImgRecordService extends IService<ImgRecord>
{

}
