package com.ruoyi.project.jxy.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.jxy.mapper.AsrRecordMapper;
import com.ruoyi.project.jxy.domain.AsrRecord;
import com.ruoyi.project.jxy.service.IAsrRecordService;

/**
 * 语音识别记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-10
 */
@Service
public class AsrRecordServiceImpl extends ServiceImpl<AsrRecordMapper, AsrRecord> implements IAsrRecordService
{
    /**
     * @return java.util.List
     * @Description //首页 按主题统计数据
     * @Date 2020/9/17 15:58
     * @Param []
     **/
    @Override
    public List getThemeChartData() {
        return baseMapper.getThemeChartData();
    }
}
