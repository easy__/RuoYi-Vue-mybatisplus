package com.ruoyi.project.jxy.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.ZipUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.Arrays;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.project.jxy.domain.AsrRecord;
import com.ruoyi.project.jxy.domain.DownloadHistory;
import com.ruoyi.project.jxy.service.IDownloadHistoryService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.jxy.domain.DictionaryRecord;
import com.ruoyi.project.jxy.service.IDictionaryRecordService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 词典百科记录Controller
 *
 * @author ruoyi
 * @date 2020-09-07
 */
@RestController
@RequestMapping("/jxy/dictionaryRecord")
public class DictionaryRecordController extends BaseController {
    @Autowired
    private IDictionaryRecordService dictionaryRecordService;
    @Autowired
    private IDownloadHistoryService downloadHistoryService;

    /**
     * 查询词典百科记录列表
     */
    @PreAuthorize("@ss.hasPermi('jxy:dictionaryRecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(DictionaryRecord dictionaryRecord) {
        startPage();
        QueryWrapper<DictionaryRecord> queryWrapper = new QueryWrapper<DictionaryRecord>();
        if (StringUtils.isNotBlank(dictionaryRecord.getRegion())) {
            queryWrapper.eq("region", dictionaryRecord.getRegion());
        }
        if (StringUtils.isNotBlank(dictionaryRecord.getTheme())) {
            queryWrapper.eq("theme", dictionaryRecord.getTheme());
        }
        if (StringUtils.isNotBlank(dictionaryRecord.getName())) {
            queryWrapper.like("name", dictionaryRecord.getName());
        }
        if (StringUtils.isNotBlank(dictionaryRecord.getSex())) {
            queryWrapper.eq("sex", dictionaryRecord.getSex());
        }
        if (dictionaryRecord.getAge() != null) {
            queryWrapper.eq("age", dictionaryRecord.getAge());
        }
        if (StringUtils.isNotBlank(dictionaryRecord.getDisplayName())) {
            queryWrapper.like("display_name", dictionaryRecord.getDisplayName());
        }
        List<DictionaryRecord> list = dictionaryRecordService.list(queryWrapper);
        return getDataTable(list);
    }

    /**
     * 导出词典百科记录列表
     */
    @PreAuthorize("@ss.hasPermi('jxy:dictionaryRecord:export')")
    @Log(title = "词典百科记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DictionaryRecord dictionaryRecord) {
        QueryWrapper<DictionaryRecord> queryWrapper = new QueryWrapper<DictionaryRecord>(dictionaryRecord);
        List<DictionaryRecord> list = dictionaryRecordService.list(queryWrapper);
        ExcelUtil<DictionaryRecord> util = new ExcelUtil<DictionaryRecord>(DictionaryRecord.class);
        return util.exportExcel(list, "dictionaryRecord");
    }

    /**
     * 获取词典百科记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('jxy:dictionaryRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(dictionaryRecordService.getById(id));
    }

    /**
     * 新增词典百科记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:dictionaryRecord:add')")
    @Log(title = "词典百科记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DictionaryRecord dictionaryRecord) {
        dictionaryRecord.setCreateTime(new Date());
        dictionaryRecord.setCreateUser(SecurityUtils.getUsername());
        String md5 = DigestUtils.md5Hex(FileUtil.readBytes(FileUtil.newFile(dictionaryRecord.getPath().replace("/profile", RuoYiConfig.getProfile()))));
        dictionaryRecord.setFileMd5(md5);
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("file_md5", dictionaryRecord.getFileMd5());
        int check = dictionaryRecordService.count(wrapper);
        if (check != 0) {
            return AjaxResult.error("该文件已存在");
        }
        return toAjax(dictionaryRecordService.save(dictionaryRecord) ? 1 : 0);
    }

    /**
     * 修改词典百科记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:dictionaryRecord:edit')")
    @Log(title = "词典百科记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DictionaryRecord dictionaryRecord) {
        String md5 = DigestUtils.md5Hex(FileUtil.readBytes(FileUtil.newFile(dictionaryRecord.getPath().replace("/profile", RuoYiConfig.getProfile()))));
        dictionaryRecord.setFileMd5(md5);
        dictionaryRecord.setUpdateTime(new Date());
        dictionaryRecord.setUpdateUser(SecurityUtils.getUsername());
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("file_md5", dictionaryRecord.getFileMd5());
        wrapper.ne("id", dictionaryRecord.getId());
        int check = dictionaryRecordService.count(wrapper);
        if (check != 0) {
            return AjaxResult.error("该文件已存在");
        }

        return toAjax(dictionaryRecordService.updateById(dictionaryRecord) ? 1 : 0);
    }

    /**
     * 删除词典百科记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:dictionaryRecord:remove')")
    @Log(title = "词典百科记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(dictionaryRecordService.removeByIds(Arrays.asList(ids)) ? 1 : 0);
    }

    @PreAuthorize("@ss.hasPermi('jxy:dictionaryRecord:zip')")
    @Log(title = "词典百科打包", businessType = BusinessType.OTHER)
    @GetMapping("/zip")
    public AjaxResult zip(DictionaryRecord dictionaryRecord) {
        QueryWrapper<DictionaryRecord> queryWrapper = new QueryWrapper<DictionaryRecord>();
        if (StringUtils.isNotBlank(dictionaryRecord.getRegion())) {
            queryWrapper.eq("region", dictionaryRecord.getRegion());
        }
        if (StringUtils.isNotBlank(dictionaryRecord.getTheme())) {
            queryWrapper.eq("theme", dictionaryRecord.getTheme());
        }
        if (StringUtils.isNotBlank(dictionaryRecord.getName())) {
            queryWrapper.like("name", dictionaryRecord.getName());
        }
        if (StringUtils.isNotBlank(dictionaryRecord.getSex())) {
            queryWrapper.eq("sex", dictionaryRecord.getSex());
        }
        if (dictionaryRecord.getAge() != null) {
            queryWrapper.eq("age", dictionaryRecord.getAge());
        }
        if (StringUtils.isNotBlank(dictionaryRecord.getDisplayName())) {
            queryWrapper.like("display_name", dictionaryRecord.getDisplayName());
        }
        List<DictionaryRecord> list = dictionaryRecordService.list(queryWrapper);
        DownloadHistory downloadHistory = new DownloadHistory();
        BeanUtil.copyProperties(dictionaryRecord, downloadHistory);
        downloadHistory.setId(null);
        downloadHistory.setType("5");
        downloadHistory.setCreateUser(SecurityUtils.getUsername());
        downloadHistory.setCreateTime(new Date());
        downloadHistoryService.save(downloadHistory);
        ThreadUtil.execute(new Runnable() {
            @Override
            public void run() {
                String baseDir = RuoYiConfig.getProfile();
                //打包的文件夹名称
                String fileName = DateUtils.dateTimeNow();
                String fileDir = baseDir + "/download/" + fileName;
                String wavPath = fileDir + "/wav";
                String txtPath = fileDir + "/txt";
                //新建wav文件夹
                FileUtil.mkdir(wavPath);
                //新建txt 文件夹
                FileUtil.mkdir(txtPath);
                for (DictionaryRecord record :
                        list) {
                    //复制文件
                    //每一个文件的真实地址
                    String realFilePath = record.getPath().replace("/profile", baseDir);
                    //复制音频文件
                    FileUtil.copy(realFilePath, wavPath, true);
                    //获取文件名
                    String name = record.getDisplayName().substring(0, record.getDisplayName().indexOf("."));
                    //生成txt 文件
                    FileUtil.writeString(record.getContent(), FileUtil.newFile(txtPath + "/" + name + ".txt"), Charset.defaultCharset());
                }
                File zip = ZipUtil.zip(fileDir);
                //压缩包真实名称
                String zipPath = zip.getAbsolutePath();
                String zipName = zip.getName();
                //转换为可下载路径
                String downloadPath = zipPath.replace(baseDir, "/profile");
                downloadHistory.setPath(downloadPath);
                downloadHistory.setDisplayName(zipName);
                downloadHistoryService.updateById(downloadHistory);
            }
        });
        return AjaxResult.success();
    }
}
