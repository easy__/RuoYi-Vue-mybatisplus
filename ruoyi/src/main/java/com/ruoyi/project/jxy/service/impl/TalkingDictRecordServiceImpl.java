package com.ruoyi.project.jxy.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.jxy.mapper.TalkingDictRecordMapper;
import com.ruoyi.project.jxy.domain.TalkingDictRecord;
import com.ruoyi.project.jxy.service.ITalkingDictRecordService;

/**
 * 发音词典Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-09
 */
@Service
public class TalkingDictRecordServiceImpl extends ServiceImpl<TalkingDictRecordMapper, TalkingDictRecord> implements ITalkingDictRecordService
{

}
