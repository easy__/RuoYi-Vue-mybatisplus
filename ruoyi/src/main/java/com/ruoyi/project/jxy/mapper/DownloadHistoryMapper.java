package com.ruoyi.project.jxy.mapper;

import com.ruoyi.project.jxy.domain.DownloadHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 下载记录Mapper接口
 * 
 * @author ruoyi
 * @date 2020-08-25
 */
public interface DownloadHistoryMapper extends BaseMapper<DownloadHistory>
{

}
