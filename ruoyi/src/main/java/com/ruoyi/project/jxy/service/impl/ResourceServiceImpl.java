package com.ruoyi.project.jxy.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.jxy.mapper.ResourceMapper;
import com.ruoyi.project.jxy.domain.Resource;
import com.ruoyi.project.jxy.service.IResourceService;

/**
 * 资源管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-05-21
 */
@Service
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements IResourceService
{

}
