package com.ruoyi.project.jxy.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.util.Date;
import java.util.List;
import java.util.Arrays;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.framework.config.ServerConfig;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.jxy.domain.FileHistory;
import com.ruoyi.project.jxy.service.IFileHistoryService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件记录Controller
 *
 * @author ruoyi
 * @date 2020-08-06
 */
@RestController
@RequestMapping("/jxy/FileHistory")
public class FileHistoryController extends BaseController {
    @Autowired
    private IFileHistoryService fileHistoryService;
    @Autowired
    private ServerConfig serverConfig;

    /**
     * 查询文件记录列表
     */
    @PreAuthorize("@ss.hasPermi('jxy:FileHistory:list')")
    @GetMapping("/list")
    public TableDataInfo list(FileHistory fileHistory) {
        startPage();
        QueryWrapper<FileHistory> queryWrapper = new QueryWrapper<FileHistory>();
        if (StringUtils.isNotBlank(fileHistory.getType())) {
            queryWrapper.eq("type", fileHistory.getType());
        }
        if (StringUtils.isNotBlank(fileHistory.getRegion())) {
            queryWrapper.eq("region", fileHistory.getRegion());
        }
        if (StringUtils.isNotBlank(fileHistory.getTheme())) {
            queryWrapper.eq("theme", fileHistory.getTheme());
        }
        if (StringUtils.isNotBlank(fileHistory.getSex())) {
            queryWrapper.eq("sex", fileHistory.getSex());
        }
        if (fileHistory.getAge() != null) {
            queryWrapper.eq("age", fileHistory.getAge());
        }
        if (StringUtils.isNotBlank(fileHistory.getPath())) {
            queryWrapper.eq("path", fileHistory.getPath());
        }
        if (StringUtils.isNotBlank(fileHistory.getDisplayName())) {
            queryWrapper.like("display_name", fileHistory.getDisplayName());
        }
        if (StringUtils.isNotBlank(fileHistory.getFileMd5())) {
            queryWrapper.eq("file_md5", fileHistory.getFileMd5());
        }
        if (StringUtils.isNotBlank(fileHistory.getZipFlag())) {
            queryWrapper.eq("zip_flag", fileHistory.getZipFlag());
        }
        if (StringUtils.isNotBlank(fileHistory.getCreateUser())) {
            queryWrapper.eq("create_user", fileHistory.getCreateUser());
        }
        if (StringUtils.isNotBlank(fileHistory.getUpdateUser())) {
            queryWrapper.eq("update_user", fileHistory.getUpdateUser());
        }
        List<FileHistory> list = fileHistoryService.list(queryWrapper);
        return getDataTable(list);
    }

    /**
     * 导出文件记录列表
     */
    @PreAuthorize("@ss.hasPermi('jxy:FileHistory:export')")
    @Log(title = "文件记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FileHistory fileHistory) {
        QueryWrapper<FileHistory> queryWrapper = new QueryWrapper<FileHistory>(fileHistory);
        List<FileHistory> list = fileHistoryService.list(queryWrapper);
        ExcelUtil<FileHistory> util = new ExcelUtil<FileHistory>(FileHistory.class);
        return util.exportExcel(list, "FileHistory");
    }

    /**
     * 获取文件记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('jxy:FileHistory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fileHistoryService.getById(id));
    }

    /**
     * 新增文件记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:FileHistory:add')")
    @Log(title = "文件记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FileHistory fileHistory) {
        fileHistory.setCreateTime(new Date());
        fileHistory.setCreateUser(SecurityUtils.getUsername());
        //未解压
        fileHistory.setZipFlag("0");
        return toAjax(fileHistoryService.save(fileHistory) ? 1 : 0);
    }

    /**
     * 修改文件记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:FileHistory:edit')")
    @Log(title = "文件记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FileHistory fileHistory) {
        return toAjax(fileHistoryService.updateById(fileHistory) ? 1 : 0);
    }

    /**
     * 删除文件记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:FileHistory:remove')")
    @Log(title = "文件记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fileHistoryService.removeByIds(Arrays.asList(ids)) ? 1 : 0);
    }

    /**
     * 通用上传请求
     */
    @PostMapping("/upload")
    public AjaxResult uploadFile(MultipartFile file) throws Exception {
        try {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * @return com.ruoyi.framework.web.domain.AjaxResult
     * @Description //解压文件
     * @Date 2020/8/13 15:52
     * @Param [id]
     **/
    @PreAuthorize("@ss.hasPermi('jxy:FileHistory:unZip')")
    @PostMapping("/unZip")
    public AjaxResult unZip(@RequestBody FileHistory fileHistory) {
        logger.info("id is :"+fileHistory.getId());
        return toAjax(fileHistoryService.unZip(fileHistory.getId()));
    }
}
