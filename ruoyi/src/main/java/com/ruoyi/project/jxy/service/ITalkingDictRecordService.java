package com.ruoyi.project.jxy.service;

import com.ruoyi.project.jxy.domain.TalkingDictRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 发音词典Service接口
 * 
 * @author ruoyi
 * @date 2020-09-09
 */
public interface ITalkingDictRecordService extends IService<TalkingDictRecord>
{

}
