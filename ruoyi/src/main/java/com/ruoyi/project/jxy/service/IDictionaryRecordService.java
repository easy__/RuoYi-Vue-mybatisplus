package com.ruoyi.project.jxy.service;

import com.ruoyi.project.jxy.domain.DictionaryRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 词典百科记录Service接口
 * 
 * @author ruoyi
 * @date 2020-09-07
 */
public interface IDictionaryRecordService extends IService<DictionaryRecord>
{

}
