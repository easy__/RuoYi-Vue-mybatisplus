package com.ruoyi.project.jxy.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 文件记录对象 tb_file_history
 *
 * @author ruoyi
 * @date 2020-08-11
 */
public class FileHistory {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 资源类型
     */
    @Excel(name = "资源类型")
    private String type;

    /**
     * 区域
     */
    @Excel(name = "区域")
    private String region;

    @Excel(name = "姓名")
    private String name;

    /**
     * 主题
     */
    @Excel(name = "主题")
    private String theme;

    @Excel(name = "语言类型")
    private String languageType;

    /**
     * $column.columnComment
     */
    @Excel(name = "性别")
    private String sex;

    /**
     * $column.columnComment
     */
    @Excel(name = "年龄")
    private Long age;

    /**
     * 文件存储路径
     */
    @Excel(name = "文件存储路径")
    private String path;

    /**
     * 显示名称
     */
    @Excel(name = "显示名称")
    private String displayName;

    /**
     * $column.columnComment
     */
    @Excel(name = "显示名称")
    private String fileMd5;

    /**
     * $column.columnComment
     */
    @Excel(name = "显示名称")
    private String zipFlag;

    /**
     * $column.columnComment
     */
    private Date createTime;

    /**
     * $column.columnComment
     */
    @Excel(name = "显示名称")
    private String createUser;

    /**
     * $column.columnComment
     */
    private Date updateTime;

    /**
     * $column.columnComment
     */
    @Excel(name = "显示名称")
    private String updateUser;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getTheme() {
        return theme;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Long getAge() {
        return age;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setFileMd5(String fileMd5) {
        this.fileMd5 = fileMd5;
    }

    public String getFileMd5() {
        return fileMd5;
    }

    public void setZipFlag(String zipFlag) {
        this.zipFlag = zipFlag;
    }

    public String getZipFlag() {
        return zipFlag;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public String getLanguageType() {
        return languageType;
    }

    public void setLanguageType(String languageType) {
        this.languageType = languageType;
    }

    @Override
    public String toString() {
        return "FileHistory{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", region='" + region + '\'' +
                ", name='" + name + '\'' +
                ", theme='" + theme + '\'' +
                ", languageType='" + languageType + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", path='" + path + '\'' +
                ", displayName='" + displayName + '\'' +
                ", fileMd5='" + fileMd5 + '\'' +
                ", zipFlag='" + zipFlag + '\'' +
                ", createTime=" + createTime +
                ", createUser='" + createUser + '\'' +
                ", updateTime=" + updateTime +
                ", updateUser='" + updateUser + '\'' +
                '}';
    }
}