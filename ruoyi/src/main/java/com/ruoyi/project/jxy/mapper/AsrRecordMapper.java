package com.ruoyi.project.jxy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.jxy.domain.AsrRecord;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 语音识别记录Mapper接口
 * 
 * @author ruoyi
 * @date 2020-08-10
 */
public interface AsrRecordMapper extends BaseMapper<AsrRecord>
{
    @Select("SELECT  A.dict_label,IFNULL(B.total,0) total FROM sys_dict_data A  LEFT JOIN (" +
            "SELECT COUNT(*) total,theme FROM (SELECT theme FROM tb_asr_record UNION all SELECT theme FROM tb_tts_record " +
            "UNION all SELECT theme FROM tb_img_record " +
            "UNION all SELECT theme FROM tb_dictionary_record " +
            "UNION all SELECT theme FROM tb_talking_dict_record ) A GROUP BY A.theme " +
            ")B    ON A.dict_value= B.theme " +
            "WHERE A.dict_type ='sys_resource_theme' ")
    List<Map> getThemeChartData();
}
