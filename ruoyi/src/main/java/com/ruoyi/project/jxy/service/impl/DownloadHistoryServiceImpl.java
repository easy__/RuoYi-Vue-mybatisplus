package com.ruoyi.project.jxy.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.jxy.mapper.DownloadHistoryMapper;
import com.ruoyi.project.jxy.domain.DownloadHistory;
import com.ruoyi.project.jxy.service.IDownloadHistoryService;

/**
 * 下载记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-25
 */
@Service
public class DownloadHistoryServiceImpl extends ServiceImpl<DownloadHistoryMapper, DownloadHistory> implements IDownloadHistoryService
{

}
