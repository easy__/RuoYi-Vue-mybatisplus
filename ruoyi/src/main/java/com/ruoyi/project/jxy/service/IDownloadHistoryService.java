package com.ruoyi.project.jxy.service;

import com.ruoyi.project.jxy.domain.DownloadHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 下载记录Service接口
 * 
 * @author ruoyi
 * @date 2020-08-25
 */
public interface IDownloadHistoryService extends IService<DownloadHistory>
{

}
