package com.ruoyi.project.jxy.service;

import com.ruoyi.project.jxy.domain.AlignedCorpusRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 对齐语料Service接口
 * 
 * @author ruoyi
 * @date 2020-09-09
 */
public interface IAlignedCorpusRecordService extends IService<AlignedCorpusRecord>
{

}
