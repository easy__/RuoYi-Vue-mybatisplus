package com.ruoyi.project.jxy.mapper;

import com.ruoyi.project.jxy.domain.TalkingDictRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 发音词典Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-09
 */
public interface TalkingDictRecordMapper extends BaseMapper<TalkingDictRecord>
{

}
