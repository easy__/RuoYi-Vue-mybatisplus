package com.ruoyi.project.jxy.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.util.List;
import java.util.Arrays;

import com.ruoyi.common.utils.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.jxy.domain.DownloadHistory;
import com.ruoyi.project.jxy.service.IDownloadHistoryService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 下载记录Controller
 *
 * @author ruoyi
 * @date 2020-08-25
 */
@RestController
@RequestMapping("/jxy/downloadHistory")
public class DownloadHistoryController extends BaseController {
    @Autowired
    private IDownloadHistoryService downloadHistoryService;

    /**
     * 查询下载记录列表
     */
    @PreAuthorize("@ss.hasPermi('jxy:downloadHistory:list')")
    @GetMapping("/list")
    public TableDataInfo list(DownloadHistory downloadHistory) {
        startPage();
        QueryWrapper<DownloadHistory> queryWrapper = new QueryWrapper<DownloadHistory>();
        if (StringUtils.isNotBlank(downloadHistory.getType())) {
            queryWrapper.eq("type", downloadHistory.getType());
        }
        if (StringUtils.isNotBlank(downloadHistory.getRegion())) {
            queryWrapper.eq("region", downloadHistory.getRegion());
        }
        if (StringUtils.isNotBlank(downloadHistory.getTheme())) {
            queryWrapper.eq("theme", downloadHistory.getTheme());
        }
        if (StringUtils.isNotBlank(downloadHistory.getName())) {
            queryWrapper.like("name", downloadHistory.getName());
        }
        if (StringUtils.isNotBlank(downloadHistory.getSex())) {
            queryWrapper.eq("sex", downloadHistory.getSex());
        }
        if (downloadHistory.getAge() != null) {
            queryWrapper.eq("age", downloadHistory.getAge());
        }
        if (StringUtils.isNotBlank(downloadHistory.getPath())) {
            queryWrapper.eq("path", downloadHistory.getPath());
        }
        if (StringUtils.isNotBlank(downloadHistory.getDisplayName())) {
            queryWrapper.like("display_name", downloadHistory.getDisplayName());
        }
        if (StringUtils.isNotBlank(downloadHistory.getCreateUser())) {
            queryWrapper.eq("create_user", downloadHistory.getCreateUser());
        }
        if (StringUtils.isNotBlank(downloadHistory.getUpdateUser())) {
            queryWrapper.eq("update_user", downloadHistory.getUpdateUser());
        }
        List<DownloadHistory> list = downloadHistoryService.list(queryWrapper);
        return getDataTable(list);
    }

    /**
     * 导出下载记录列表
     */
    @PreAuthorize("@ss.hasPermi('jxy:downloadHistory:export')")
    @Log(title = "下载记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DownloadHistory downloadHistory) {
        QueryWrapper<DownloadHistory> queryWrapper = new QueryWrapper<DownloadHistory>(downloadHistory);
        List<DownloadHistory> list = downloadHistoryService.list(queryWrapper);
        ExcelUtil<DownloadHistory> util = new ExcelUtil<DownloadHistory>(DownloadHistory.class);
        return util.exportExcel(list, "downloadHistory");
    }

    /**
     * 获取下载记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('jxy:downloadHistory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(downloadHistoryService.getById(id));
    }

    /**
     * 新增下载记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:downloadHistory:add')")
    @Log(title = "下载记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DownloadHistory downloadHistory) {
        return toAjax(downloadHistoryService.save(downloadHistory) ? 1 : 0);
    }

    /**
     * 修改下载记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:downloadHistory:edit')")
    @Log(title = "下载记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DownloadHistory downloadHistory) {
        return toAjax(downloadHistoryService.updateById(downloadHistory) ? 1 : 0);
    }

    /**
     * 删除下载记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:downloadHistory:remove')")
    @Log(title = "下载记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(downloadHistoryService.removeByIds(Arrays.asList(ids)) ? 1 : 0);
    }
}
