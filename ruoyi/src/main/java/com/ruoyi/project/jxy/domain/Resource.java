package com.ruoyi.project.jxy.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 资源管理对象 tb_resource
 *
 * @author ruoyi
 * @date 2020-05-21
 */

public class Resource {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 资源类型
     */
    @ApiModelProperty("资源类型")
    @Excel(name = "资源类型")
    private String type;

    /**
     * 区域
     */
    @Excel(name = "区域")
    private String region;

    /**
     * 主题
     */
    @Excel(name = "主题")
    private String theme;

    /**
     * 性别
     */
    @Excel(name = "性别")
    private String sex;

    /**
     * 年龄
     */
    @Excel(name = "年龄")
    private Long age;

    /**
     * 类型 0 文件 1文件夹
     */
    @ApiModelProperty("文件/文件夹类型 0 文件 1文件夹")
    @Excel(name = "类型 0 文件 1文件夹")
    private String folderType;

    /**
     * 显示的文件夹/文件名称
     */
    @ApiModelProperty("显示的文件夹/文件名称")
    private String displayName;

    /**
     * 实际存储的文件夹/文件名称
     */
    @ApiModelProperty("实际存储的文件夹/文件名称")
    @Excel(name = "实际存储的文件夹/文件名称")
    private String path;

    /**
     * 父文件夹id
     */
    @ApiModelProperty("父文件夹id")
    @Excel(name = "父文件夹id")
    private Long parentId;

    /**
     * $column.columnComment
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * $column.columnComment
     */
    @Excel(name = "创建人")
    private String createUser;

    /**
     * $column.columnComment
     */
    private Date updateTime;

    /**
     * $column.columnComment
     */
    @Excel(name = "更新人")
    private String updateUser;
    @TableField(exist = false)
    private boolean isFolder;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getTheme() {
        return theme;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Long getAge() {
        return age;
    }

    public void setFolderType(String folderType) {
        this.folderType = folderType;
    }

    public String getFolderType() {
        return folderType;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean getIsFolder() {
        return isFolder;
    }

    public void setIsFolder(boolean isFolder) {
        this.isFolder = isFolder;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id" , getId())
                .append("type" , getType())
                .append("region" , getRegion())
                .append("theme" , getTheme())
                .append("sex" , getSex())
                .append("age" , getAge())
                .append("folderType" , getFolderType())
                .append("﻿﻿displayName" , getDisplayName())
                .append("path" , getPath())
                .append("parentId" , getParentId())
                .append("createTime" , getCreateTime())
                .append("createUser" , getCreateUser())
                .append("updateTime" , getUpdateTime())
                .append("updateUser" , getUpdateUser())
                .toString();
    }
}
