package com.ruoyi.project.jxy.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.jxy.domain.ImgRecord;
import com.ruoyi.project.jxy.entity.EchartsEntity;
import com.ruoyi.project.jxy.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @ClassName IndexController
 * @Description 首页统计
 * @Author zheng
 * @Date 2020/9/14 16:15
 * @Version 1.0
 **/
@RestController
@RequestMapping("/jxy/index")
public class IndexController {
    @Autowired
    private IAsrRecordService asrRecordService;
    @Autowired
    private ITtsRecordService ttsRecordService;
    @Autowired
    private IAlignedCorpusRecordService alignedCorpusRecordService;
    @Autowired
    private IImgRecordService imgRecordService;
    @Autowired
    private IDictionaryRecordService dictionaryRecordService;
    @Autowired
    private ITalkingDictRecordService talkingDictRecordService;

    /**
     * 导出图片记录列表
     */
    @PreAuthorize("@ss.hasPermi('jxy:index:getCharData')")
    @Log(title = "首页统计", businessType = BusinessType.OTHER)
    @GetMapping("/getCharData")
    public AjaxResult getCharData() {
        List<EchartsEntity> echarts = new ArrayList<EchartsEntity>();
        //语音识别
        int asrTotal = asrRecordService.count();
        //语音合成
        int ttsTotal = ttsRecordService.count();
        //发音词典
        int talkingTotal = talkingDictRecordService.count();
        //对齐语料
        int alignTotal = alignedCorpusRecordService.count();
        //图片识别
        int imgTotal = imgRecordService.count();
        //词典百科
        int dicTotal = dictionaryRecordService.count();


        AjaxResult ajax = AjaxResult.success();
        ajax.put("asrTotal", asrTotal);
        ajax.put("ttsTotal", ttsTotal);
        ajax.put("talkingTotal", talkingTotal);
        ajax.put("alignTotal", alignTotal);
        ajax.put("imgTotal", imgTotal);
        ajax.put("dicTotal", dicTotal);

        //BarChart 数据定义
        //自定义横坐标
        List<String> xAxis = new ArrayList<>();
        xAxis.add("语音识别");
        xAxis.add("语音合成");
        xAxis.add("发音词典");
        xAxis.add("对齐语料");
        xAxis.add("图片识别");
        xAxis.add("词典百科");
        //String[] xAxis = {"语音识别", "语音合成", "发音词典", "对齐语料", "图片识别", "词典百科"};
        //EchartsEntity entity = new EchartsEntity("数量", "line", Arrays.asList(asrTotal, ttsTotal, talkingTotal, alignTotal, imgTotal, dicTotal));
        //echarts.add(entity);
        ajax.put("barXAxis", xAxis);
        ajax.put("barChartData", Arrays.asList(asrTotal, ttsTotal, talkingTotal, alignTotal, imgTotal, dicTotal));
        List<String> lineXAxis = new ArrayList<>();
        List<Long> lineChartData = new ArrayList<>();
        //LineChart 数据定义
        List<Map> lst = asrRecordService.getThemeChartData();
        lst.forEach(item -> {
            String label = item.get("dict_label").toString();
            Long total = (Long) item.get("total");
            lineXAxis.add(label);
            lineChartData.add(total);
        });

        ajax.put("lineXAxis", lineXAxis);
        ajax.put("lineChartData", lineChartData);

        return ajax;
    }
}
