package com.ruoyi.project.jxy.mapper;

import com.ruoyi.project.jxy.domain.FileHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 文件记录Mapper接口
 * 
 * @author ruoyi
 * @date 2020-08-06
 */
public interface FileHistoryMapper extends BaseMapper<FileHistory>
{

}
