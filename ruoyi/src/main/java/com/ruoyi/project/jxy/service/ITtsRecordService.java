package com.ruoyi.project.jxy.service;

import com.ruoyi.project.jxy.domain.TtsRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 语音合成记录Service接口
 * 
 * @author ruoyi
 * @date 2020-09-07
 */
public interface ITtsRecordService extends IService<TtsRecord>
{

}
