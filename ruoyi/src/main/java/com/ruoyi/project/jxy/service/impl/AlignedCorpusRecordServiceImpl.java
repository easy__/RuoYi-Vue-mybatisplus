package com.ruoyi.project.jxy.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.jxy.mapper.AlignedCorpusRecordMapper;
import com.ruoyi.project.jxy.domain.AlignedCorpusRecord;
import com.ruoyi.project.jxy.service.IAlignedCorpusRecordService;

/**
 * 对齐语料Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-09
 */
@Service
public class AlignedCorpusRecordServiceImpl extends ServiceImpl<AlignedCorpusRecordMapper, AlignedCorpusRecord> implements IAlignedCorpusRecordService
{

}
