package com.ruoyi.project.jxy.service;

import com.ruoyi.project.jxy.domain.Resource;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 资源管理Service接口
 * 
 * @author ruoyi
 * @date 2020-05-21
 */
public interface IResourceService extends IService<Resource>
{

}
