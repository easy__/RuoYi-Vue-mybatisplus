package com.ruoyi.project.jxy.mapper;

import com.ruoyi.project.jxy.domain.DictionaryRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 词典百科记录Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-07
 */
public interface DictionaryRecordMapper extends BaseMapper<DictionaryRecord>
{

}
