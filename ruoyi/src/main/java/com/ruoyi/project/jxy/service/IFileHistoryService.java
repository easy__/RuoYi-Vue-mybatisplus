package com.ruoyi.project.jxy.service;

import com.ruoyi.project.jxy.domain.FileHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 文件记录Service接口
 *
 * @author ruoyi
 * @date 2020-08-06
 */
public interface IFileHistoryService extends IService<FileHistory> {
    public int unZip(Long id);
}
