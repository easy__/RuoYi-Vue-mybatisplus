package com.ruoyi.project.jxy.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.ZipUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.io.File;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Arrays;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.project.jxy.domain.DownloadHistory;
import com.ruoyi.project.jxy.service.IDownloadHistoryService;
import com.ruoyi.project.jxy.service.impl.FileInfoThread;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.jxy.domain.AsrRecord;
import com.ruoyi.project.jxy.service.IAsrRecordService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 语音识别记录Controller
 *
 * @author ruoyi
 * @date 2020-08-10
 */
@RestController
@RequestMapping("/jxy/asrRecord")
public class AsrRecordController extends BaseController {
    @Autowired
    private IAsrRecordService asrRecordService;
    @Autowired
    private IDownloadHistoryService downloadHistoryService;

    /**
     * 查询语音识别记录列表
     */
    @PreAuthorize("@ss.hasPermi('jxy:asrRecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(AsrRecord asrRecord) {
        startPage();
        QueryWrapper<AsrRecord> queryWrapper = new QueryWrapper<AsrRecord>();
        if (StringUtils.isNotBlank(asrRecord.getRegion())) {
            queryWrapper.eq("region", asrRecord.getRegion());
        }
        if (StringUtils.isNotBlank(asrRecord.getTheme())) {
            queryWrapper.eq("theme", asrRecord.getTheme());
        }
        if (StringUtils.isNotBlank(asrRecord.getName())) {
            queryWrapper.eq("name", asrRecord.getName());
        }
        if (StringUtils.isNotBlank(asrRecord.getSex())) {
            queryWrapper.eq("sex", asrRecord.getSex());
        }
        if (asrRecord.getAge() != null) {
            queryWrapper.eq("age", asrRecord.getAge());
        }
        if (StringUtils.isNotBlank(asrRecord.getDisplayName())) {
            queryWrapper.like("display_name", asrRecord.getDisplayName());
        }
        if (StringUtils.isNotBlank(asrRecord.getContent())) {
            queryWrapper.eq("content", asrRecord.getContent());
        }

        List<AsrRecord> list = asrRecordService.list(queryWrapper);
        return getDataTable(list);
    }

    /**
     * 导出语音识别记录列表
     */
    @PreAuthorize("@ss.hasPermi('jxy:asrRecord:export')")
    @Log(title = "语音识别记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AsrRecord asrRecord) {
        QueryWrapper<AsrRecord> queryWrapper = new QueryWrapper<AsrRecord>(asrRecord);
        List<AsrRecord> list = asrRecordService.list(queryWrapper);
        ExcelUtil<AsrRecord> util = new ExcelUtil<AsrRecord>(AsrRecord.class);
        return util.exportExcel(list, "asrRecord");
    }

    /**
     * 获取语音识别记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('jxy:asrRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(asrRecordService.getById(id));
    }

    /**
     * 新增语音识别记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:asrRecord:add')")
    @Log(title = "语音识别记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AsrRecord asrRecord) {
        asrRecord.setCreateTime(new Date());
        asrRecord.setCreateUser(SecurityUtils.getUsername());
        String md5 = DigestUtils.md5Hex(FileUtil.readBytes(FileUtil.newFile(asrRecord.getPath().replace("/profile", RuoYiConfig.getProfile()))));
        asrRecord.setFileMd5(md5);
        Long duration =FileInfoThread.getDuration(FileUtil.newFile(asrRecord.getPath().replace("/profile", RuoYiConfig.getProfile())));
        asrRecord.setDuration(duration);
        asrRecord.setDurationText(FileInfoThread.getDuration(duration));
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("file_md5", asrRecord.getFileMd5());
        int check = asrRecordService.count(wrapper);
        if (check != 0) {
            return AjaxResult.error("该文件已存在");
        }

        return toAjax(asrRecordService.save(asrRecord) ? 1 : 0);
    }

    /**
     * 修改语音识别记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:asrRecord:edit')")
    @Log(title = "语音识别记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AsrRecord asrRecord) {
        String md5 = DigestUtils.md5Hex(FileUtil.readBytes(FileUtil.newFile(asrRecord.getPath().replace("/profile", RuoYiConfig.getProfile()))));
        asrRecord.setFileMd5(md5);
        Long duration =FileInfoThread.getDuration(FileUtil.newFile(asrRecord.getPath().replace("/profile", RuoYiConfig.getProfile())));
        asrRecord.setDuration(duration);
        asrRecord.setDurationText(FileInfoThread.getDuration(duration));
        asrRecord.setUpdateTime(new Date());
        asrRecord.setUpdateUser(SecurityUtils.getUsername());
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("file_md5", md5);
        wrapper.ne("id", asrRecord.getId());
        int check = asrRecordService.count(wrapper);
        if (check != 0) {
            return AjaxResult.error("该文件已存在");
        }

        return toAjax(asrRecordService.updateById(asrRecord) ? 1 : 0);
    }

    /**
     * 删除语音识别记录
     */
    @PreAuthorize("@ss.hasPermi('jxy:asrRecord:remove')")
    @Log(title = "语音识别记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(asrRecordService.removeByIds(Arrays.asList(ids)) ? 1 : 0);
    }

    /**
     * 导出语音识别数据
     */
    @PreAuthorize("@ss.hasPermi('jxy:asrRecord:zip')")
    @Log(title = "语音识别打包", businessType = BusinessType.OTHER)
    @GetMapping("/zip")
    public AjaxResult zip(AsrRecord asrRecord) {
        QueryWrapper<AsrRecord> queryWrapper = new QueryWrapper<AsrRecord>();
        if (StringUtils.isNotBlank(asrRecord.getRegion())) {
            queryWrapper.eq("region", asrRecord.getRegion());
        }
        if (StringUtils.isNotBlank(asrRecord.getTheme())) {
            queryWrapper.eq("theme", asrRecord.getTheme());
        }
        if (StringUtils.isNotBlank(asrRecord.getSex())) {
            queryWrapper.eq("sex", asrRecord.getSex());
        }
        if (asrRecord.getAge() != null) {
            queryWrapper.eq("age", asrRecord.getAge());
        }
        if (StringUtils.isNotBlank(asrRecord.getDisplayName())) {
            queryWrapper.like("display_name", asrRecord.getDisplayName());
        }
        if (StringUtils.isNotBlank(asrRecord.getContent())) {
            queryWrapper.eq("content", asrRecord.getContent());
        }

        List<AsrRecord> list = asrRecordService.list(queryWrapper);
        DownloadHistory downloadHistory = new DownloadHistory();
        BeanUtil.copyProperties(asrRecord, downloadHistory);
        downloadHistory.setId(null);
        downloadHistory.setType("0");
        downloadHistory.setCreateUser(SecurityUtils.getUsername());
        downloadHistory.setCreateTime(new Date());
        downloadHistoryService.save(downloadHistory);
        ThreadUtil.execute(new Runnable() {
            @Override
            public void run() {
                String baseDir = RuoYiConfig.getProfile();
                //打包的文件夹名称
                String fileName = DateUtils.dateTimeNow();
                String fileDir = baseDir + "/download/" + fileName;
                String wavPath = fileDir + "/wav";
                String txtPath = fileDir + "/txt";
                //新建wav文件夹
                FileUtil.mkdir(wavPath);
                //新建txt 文件夹
                FileUtil.mkdir(txtPath);
                for (AsrRecord record :
                        list) {
                    //复制文件
                    //每一个文件的真实地址
                    String realFilePath = record.getPath().replace("/profile", baseDir);
                    //复制音频文件
                    FileUtil.copy(realFilePath, wavPath, true);
                    //获取文件名
                    String name = record.getDisplayName().substring(0, record.getDisplayName().indexOf("."));
                    //生成txt 文件
                    FileUtil.writeString(record.getContent(), FileUtil.newFile(txtPath + "/" + name + ".txt"), Charset.defaultCharset());
                }
                File zip = ZipUtil.zip(fileDir);
                //压缩包真实名称
                String zipPath = zip.getAbsolutePath();
                String zipName = zip.getName();
                //转换为可下载路径
                String downloadPath = zipPath.replace(baseDir, "/profile");
                downloadHistory.setPath(downloadPath);
                downloadHistory.setDisplayName(zipName);
                downloadHistoryService.updateById(downloadHistory);
            }
        });
        return AjaxResult.success();
    }
}
