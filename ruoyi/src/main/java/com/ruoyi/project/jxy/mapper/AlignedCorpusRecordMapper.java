package com.ruoyi.project.jxy.mapper;

import com.ruoyi.project.jxy.domain.AlignedCorpusRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 对齐语料Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-09
 */
public interface AlignedCorpusRecordMapper extends BaseMapper<AlignedCorpusRecord>
{

}
