package com.ruoyi.project.jxy.mapper;

import com.ruoyi.project.jxy.domain.ImgRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 图片记录Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-07
 */
public interface ImgRecordMapper extends BaseMapper<ImgRecord>
{

}
