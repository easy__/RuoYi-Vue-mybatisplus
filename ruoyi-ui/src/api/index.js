import request from '@/utils/request'

// 获取数据统计
export function getCharData() {
  return request({
    url: '/jxy/index/getCharData',
    method: 'get'
  })
}