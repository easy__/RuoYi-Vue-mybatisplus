import request from '@/utils/request'

// 查询文件记录列表
export function listFileHistory(query) {
  return request({
    url: '/jxy/FileHistory/list',
    method: 'get',
    params: query
  })
}

// 查询文件记录详细
export function getFileHistory(id) {
  return request({
    url: '/jxy/FileHistory/' + id,
    method: 'get'
  })
}

// 新增文件记录
export function addFileHistory(data) {
  return request({
    url: '/jxy/FileHistory',
    method: 'post',
    data: data
  })
}

// 修改文件记录
export function updateFileHistory(data) {
  return request({
    url: '/jxy/FileHistory',
    method: 'put',
    data: data
  })
}

// 删除文件记录
export function delFileHistory(id) {
  return request({
    url: '/jxy/FileHistory/' + id,
    method: 'delete'
  })
}

// 导出文件记录
export function exportFileHistory(query) {
  return request({
    url: '/jxy/FileHistory/export',
    method: 'get',
    params: query
  })
}

// 上传文件
export function uploadFileHistory(query) {
  return request({
    url: '/jxy/FileHistory/upload',
    method: 'get',
    params: query
  })
}

// 生效文件记录
export function unzipFileHistory(data) {
  return request({
    url: '/jxy/FileHistory/unZip',
    method: 'post',
    data: data
  })
}