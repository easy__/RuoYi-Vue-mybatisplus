import request from '@/utils/request'

// 查询语音识别记录列表
export function listAsrRecord(query) {
  return request({
    url: '/jxy/asrRecord/list',
    method: 'get',
    params: query
  })
}

// 查询语音识别记录详细
export function getAsrRecord(id) {
  return request({
    url: '/jxy/asrRecord/' + id,
    method: 'get'
  })
}

// 新增语音识别记录
export function addAsrRecord(data) {
  return request({
    url: '/jxy/asrRecord',
    method: 'post',
    data: data
  })
}

// 修改语音识别记录
export function updateAsrRecord(data) {
  return request({
    url: '/jxy/asrRecord',
    method: 'put',
    data: data
  })
}

// 删除语音识别记录
export function delAsrRecord(id) {
  return request({
    url: '/jxy/asrRecord/' + id,
    method: 'delete'
  })
}

// 导出
export function exportAsrRecord(query) {
  return request({
    url: '/jxy/asrRecord/export',
    method: 'get',
    params: query
  })
}// 导出语音识别记录
export function zipAsrRecord(query) {
  return request({
    url: '/jxy/asrRecord/zip',
    method: 'get',
    params: query
  })
}
// 上传文件
export function uploadFileHistory(data) {
  return request({
    url: '/jxy/FileHistory/upload',
    method: 'post',
    data: data
  })
}
