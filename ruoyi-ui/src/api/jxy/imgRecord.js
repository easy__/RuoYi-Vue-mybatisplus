import request from '@/utils/request'

// 查询图片记录列表
export function listImgRecord(query) {
  return request({
    url: '/jxy/imgRecord/list',
    method: 'get',
    params: query
  })
}

// 查询图片记录详细
export function getImgRecord(id) {
  return request({
    url: '/jxy/imgRecord/' + id,
    method: 'get'
  })
}

// 新增图片记录
export function addImgRecord(data) {
  return request({
    url: '/jxy/imgRecord',
    method: 'post',
    data: data
  })
}

// 修改图片记录
export function updateImgRecord(data) {
  return request({
    url: '/jxy/imgRecord',
    method: 'put',
    data: data
  })
}

// 删除图片记录
export function delImgRecord(id) {
  return request({
    url: '/jxy/imgRecord/' + id,
    method: 'delete'
  })
}

// 导出图片记录
export function exportImgRecord(query) {
  return request({
    url: '/jxy/imgRecord/export',
    method: 'get',
    params: query
  })
}
// 导出语音合成记录
export function zipImgRecord(query) {
  return request({
    url: '/jxy/imgRecord/zip',
    method: 'get',
    params: query
  })
}