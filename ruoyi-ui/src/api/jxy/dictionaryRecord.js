import request from '@/utils/request'

// 查询词典百科记录列表
export function listDictionaryRecord(query) {
  return request({
    url: '/jxy/dictionaryRecord/list',
    method: 'get',
    params: query
  })
}

// 查询词典百科记录详细
export function getDictionaryRecord(id) {
  return request({
    url: '/jxy/dictionaryRecord/' + id,
    method: 'get'
  })
}

// 新增词典百科记录
export function addDictionaryRecord(data) {
  return request({
    url: '/jxy/dictionaryRecord',
    method: 'post',
    data: data
  })
}

// 修改词典百科记录
export function updateDictionaryRecord(data) {
  return request({
    url: '/jxy/dictionaryRecord',
    method: 'put',
    data: data
  })
}

// 删除词典百科记录
export function delDictionaryRecord(id) {
  return request({
    url: '/jxy/dictionaryRecord/' + id,
    method: 'delete'
  })
}

// 导出词典百科记录
export function exportDictionaryRecord(query) {
  return request({
    url: '/jxy/dictionaryRecord/export',
    method: 'get',
    params: query
  })
}

// 导出语音识别记录
export function zipDictionaryRecord(query) {
  return request({
    url: '/jxy/dictionaryRecord/zip',
    method: 'get',
    params: query
  })
}