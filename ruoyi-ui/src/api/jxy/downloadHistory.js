import request from '@/utils/request'

// 查询下载记录列表
export function listDownloadHistory(query) {
  return request({
    url: '/jxy/downloadHistory/list',
    method: 'get',
    params: query
  })
}

// 查询下载记录详细
export function getDownloadHistory(id) {
  return request({
    url: '/jxy/downloadHistory/' + id,
    method: 'get'
  })
}

// 新增下载记录
export function addDownloadHistory(data) {
  return request({
    url: '/jxy/downloadHistory',
    method: 'post',
    data: data
  })
}

// 修改下载记录
export function updateDownloadHistory(data) {
  return request({
    url: '/jxy/downloadHistory',
    method: 'put',
    data: data
  })
}

// 删除下载记录
export function delDownloadHistory(id) {
  return request({
    url: '/jxy/downloadHistory/' + id,
    method: 'delete'
  })
}

// 导出下载记录
export function exportDownloadHistory(query) {
  return request({
    url: '/jxy/downloadHistory/export',
    method: 'get',
    params: query
  })
}