import request from '@/utils/request'

// 查询对齐语料列表
export function listAlignedCorpus(query) {
  return request({
    url: '/jxy/alignedCorpus/list',
    method: 'get',
    params: query
  })
}

// 查询对齐语料详细
export function getAlignedCorpus(id) {
  return request({
    url: '/jxy/alignedCorpus/' + id,
    method: 'get'
  })
}

// 新增对齐语料
export function addAlignedCorpus(data) {
  return request({
    url: '/jxy/alignedCorpus',
    method: 'post',
    data: data
  })
}

// 修改对齐语料
export function updateAlignedCorpus(data) {
  return request({
    url: '/jxy/alignedCorpus',
    method: 'put',
    data: data
  })
}

// 删除对齐语料
export function delAlignedCorpus(id) {
  return request({
    url: '/jxy/alignedCorpus/' + id,
    method: 'delete'
  })
}

// 导出对齐语料
export function exportAlignedCorpus(query) {
  return request({
    url: '/jxy/alignedCorpus/export',
    method: 'get',
    params: query
  })
}
// 导出对齐语料记录
export function zipAlignedCorpusRecord(query) {
  return request({
    url: '/jxy/alignedCorpus/zip',
    method: 'get',
    params: query
  })
}