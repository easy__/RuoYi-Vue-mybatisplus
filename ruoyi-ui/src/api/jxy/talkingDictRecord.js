import request from '@/utils/request'

// 查询发音词典列表
export function listTalkingDictRecord(query) {
  return request({
    url: '/jxy/talkingDictRecord/list',
    method: 'get',
    params: query
  })
}

// 查询发音词典详细
export function getTalkingDictRecord(id) {
  return request({
    url: '/jxy/talkingDictRecord/' + id,
    method: 'get'
  })
}

// 新增发音词典
export function addTalkingDictRecord(data) {
  return request({
    url: '/jxy/talkingDictRecord',
    method: 'post',
    data: data
  })
}

// 修改发音词典
export function updateTalkingDictRecord(data) {
  return request({
    url: '/jxy/talkingDictRecord',
    method: 'put',
    data: data
  })
}

// 删除发音词典
export function delTalkingDictRecord(id) {
  return request({
    url: '/jxy/talkingDictRecord/' + id,
    method: 'delete'
  })
}

// 导出发音词典
export function exportTalkingDictRecord(query) {
  return request({
    url: '/jxy/talkingDictRecord/export',
    method: 'get',
    params: query
  })
}

// 导出发音词典记录
export function zipTalkingDictRecord(query) {
  return request({
    url: '/jxy/talkingDictRecord/zip',
    method: 'get',
    params: query
  })
}