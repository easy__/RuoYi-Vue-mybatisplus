import request from '@/utils/request'

// 查询语音合成记录列表
export function listTtsRecord(query) {
  return request({
    url: '/jxy/ttsRecord/list',
    method: 'get',
    params: query
  })
}

// 查询语音合成记录详细
export function getTtsRecord(id) {
  return request({
    url: '/jxy/ttsRecord/' + id,
    method: 'get'
  })
}

// 新增语音合成记录
export function addTtsRecord(data) {
  return request({
    url: '/jxy/ttsRecord',
    method: 'post',
    data: data
  })
}

// 修改语音合成记录
export function updateTtsRecord(data) {
  return request({
    url: '/jxy/ttsRecord',
    method: 'put',
    data: data
  })
}

// 删除语音合成记录
export function delTtsRecord(id) {
  return request({
    url: '/jxy/ttsRecord/' + id,
    method: 'delete'
  })
}
// 导出语音合成记录
export function zipTtsRecord(query) {
  return request({
    url: '/jxy/ttsRecord/zip',
    method: 'get',
    params: query
  })
}
// 导出语音合成记录
export function exportTtsRecord(query) {
  return request({
    url: '/jxy/ttsRecord/export',
    method: 'get',
    params: query
  })
}